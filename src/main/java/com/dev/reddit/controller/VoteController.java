package com.dev.reddit.controller;

import com.dev.reddit.dto.VoteDto;
import com.dev.reddit.service.VoteService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.dev.reddit.constants.Message.VOTE_SUCCESS;

@RestController
@RequestMapping("/api/votes")
@AllArgsConstructor
public class VoteController {
    private final VoteService voteService;

    @PostMapping
    public ResponseEntity<String> vote(@RequestBody VoteDto voteDto) {
        this.voteService.vote(voteDto);
        return ResponseEntity.status(HttpStatus.OK).body(VOTE_SUCCESS);
    }
}
