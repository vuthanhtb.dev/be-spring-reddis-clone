package com.dev.reddit.controller;

import com.dev.reddit.dto.CommentsDto;
import com.dev.reddit.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.dev.reddit.constants.Message.COMMENT_CREATED_SUCCESS;

@RestController
@RequestMapping("/api/comments")
@AllArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @PostMapping
    public ResponseEntity<String> createComment(@RequestBody CommentsDto commentsDto) {
        this.commentService.save(commentsDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(COMMENT_CREATED_SUCCESS);
    }

    @GetMapping("/by-post/{postId}")
    public ResponseEntity<List<CommentsDto>> findAllCommentsForPost(@PathVariable Long postId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(this.commentService.findAllCommentsForPost(postId));
    }

    @GetMapping("/by-user/{userName}")
    public ResponseEntity<List<CommentsDto>> findAllCommentsForUser(@PathVariable String userName) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(this.commentService.findAllCommentsForUser(userName));
    }
}
