package com.dev.reddit.controller;

import com.dev.reddit.dto.request.LoginRequest;
import com.dev.reddit.dto.request.RefreshTokenRequest;
import com.dev.reddit.dto.request.RegisterRequest;
import com.dev.reddit.dto.response.AuthenticationResponse;
import com.dev.reddit.service.AuthService;
import com.dev.reddit.service.RefreshTokenService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.dev.reddit.constants.Message.*;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final RefreshTokenService refreshTokenService;

    @ApiOperation(value = "Đăng ký tài khoản mới")
    @PostMapping("/register")
    public ResponseEntity<String> registering(@RequestBody RegisterRequest request) {
        this.authService.registering(request);
        return ResponseEntity.ok(USER_REGISTRATION_SUCCESS);
    }

    @GetMapping("/account-verification/{token}")
    public ResponseEntity<String> verifyAccount(@PathVariable String token) {
        this.authService.verifyAccount(token);
        return ResponseEntity.ok(USER_ACTIVATED_SUCCESS);
    }

    @ApiOperation(value = "Đăng nhập")
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest request) throws Exception{
        AuthenticationResponse response = this.authService.login(request);

        if (null == response) {
            return ResponseEntity.internalServerError().body("Login failed");
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/refresh/token")
    public ResponseEntity<AuthenticationResponse> refreshTokens(@Valid @RequestBody RefreshTokenRequest request) {
        return ResponseEntity.ok(this.authService.refreshToken(request));
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
        return ResponseEntity.ok(REFRESH_TOKEN_DELETED);
    }
}
