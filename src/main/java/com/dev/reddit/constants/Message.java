package com.dev.reddit.constants;

public class Message {
    public static String USER_REGISTRATION_SUCCESS = "User Registration Successful";
    public static String USER_ACTIVATED_SUCCESS = "Account activated Successful";
    public static String POST_CREATED_SUCCESS = "Post Created Successfully";
    public static String COMMENT_CREATED_SUCCESS = "Comment Created Successfully";
    public static String VOTE_SUCCESS = "Successfully Vote";
    public static String REFRESH_TOKEN_DELETED = "Refresh Token Deleted Successfully";
}
