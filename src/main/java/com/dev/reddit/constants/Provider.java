package com.dev.reddit.constants;

public class Provider {
    public static String ISSUER = "self";
    public static String CLAIM_NAME = "scope";
    public static String CLAIM_VALUE = "ROLE_USER";
}
