package com.dev.reddit.constants;

public class Email {
    public static String TEMPLATE_ENGINE = "mailTemplate";
    public static String FROM_EMAIL = "vuthanhtb.dev@gmail.com";
    public static String EMAIL_SUBJECT = "Please Activate your Account";
    public static String EMAIL_BODY = "Thank you for signing up to Spring Reddit, please click on the below url to activate your account: http://localhost:8080/api/auth/account-verification/%s";
}
