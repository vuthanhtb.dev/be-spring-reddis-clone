package com.dev.reddit.service;

import com.dev.reddit.dto.CommentsDto;
import com.dev.reddit.exceptions.PostNotFoundException;
import com.dev.reddit.exceptions.SpringRedditException;
import com.dev.reddit.mapper.CommentMapper;
import com.dev.reddit.model.Comment;
import com.dev.reddit.model.NotificationEmail;
import com.dev.reddit.model.Post;
import com.dev.reddit.model.User;
import com.dev.reddit.repository.CommentRepository;
import com.dev.reddit.repository.PostRepository;
import com.dev.reddit.repository.UserRepository;
import com.dev.reddit.service.mail.MailContentBuilder;
import com.dev.reddit.service.mail.MailService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class CommentService {
    private static final String POST_URL = "";
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final CommentMapper commentMapper;
    private final CommentRepository commentRepository;
    private final MailContentBuilder mailContentBuilder;
    private final MailService mailService;

    public void save(CommentsDto commentsDto) {
        Post post = this.postRepository
                .findById(commentsDto.getPostId())
                .orElseThrow(() -> new PostNotFoundException(commentsDto.getPostId().toString()));
        Comment comment = this.commentMapper.map(commentsDto, post, this.authService.getCurrentUser());
        this.commentRepository.save(comment);

        String message = this.mailContentBuilder
                .build(post.getUser().getUsername() + " posted a comment on your post." + POST_URL);
        this.sendCommentNotification(message, post.getUser());
    }

    private void sendCommentNotification(String message, User user) {
        this.mailService.sendMail(new NotificationEmail(
                user.getUsername() + " Commented on your post",
                user.getEmail(),
                message
        ));
    }

    public List<CommentsDto> findAllCommentsForPost(Long postId) {
        Post post = this.postRepository
                .findById(postId)
                .orElseThrow(() -> new PostNotFoundException(postId.toString()));
        return this.commentRepository
                .findByPost(post)
                .stream()
                .map(this.commentMapper::mapToDto).collect(toList());
    }

    public List<CommentsDto> findAllCommentsForUser(String userName) {
        User user = this.userRepository
                .findByUsername(userName)
                .orElseThrow(() -> new UsernameNotFoundException(userName));
        return this.commentRepository
                .findAllByUser(user)
                .stream()
                .map(this.commentMapper::mapToDto)
                .collect(toList());
    }

    public boolean containsSwearWords(String comment) {
        if (comment.contains("shit")) {
            throw new SpringRedditException("Comments contains unacceptable language");
        }
        return false;
    }
}
