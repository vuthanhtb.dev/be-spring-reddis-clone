package com.dev.reddit.service;

import com.dev.reddit.exceptions.SpringRedditException;
import com.dev.reddit.model.RefreshToken;
import com.dev.reddit.repository.RefreshTokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;

@Service
@AllArgsConstructor
@Transactional
public class RefreshTokenService {
    private final RefreshTokenRepository refreshTokenRepository;

    public RefreshToken generateRefreshToken() {
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken.setCreatedDate(Instant.now());

        return this.refreshTokenRepository.save(refreshToken);
    }

    void validateRefreshToken(String token) {
        this.refreshTokenRepository
                .findByToken(token)
                .orElseThrow(() -> new SpringRedditException("Invalid refresh Token"));
    }

    public void deleteRefreshToken(String token) {
        this.refreshTokenRepository.deleteByToken(token);
    }
}
