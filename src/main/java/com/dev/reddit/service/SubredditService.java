package com.dev.reddit.service;

import com.dev.reddit.dto.SubredditDto;
import com.dev.reddit.exceptions.SubredditNotFoundException;
import com.dev.reddit.mapper.SubredditMapper;
import com.dev.reddit.model.Subreddit;
import com.dev.reddit.repository.SubredditRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class SubredditService {

    private final SubredditRepository subredditRepository;
    private final SubredditMapper subredditMapper;

    @Transactional
    public SubredditDto save(SubredditDto subredditDto) {
        Subreddit subreddit = this.subredditRepository.save(this.subredditMapper.mapDtoToSubreddit(subredditDto));
        subredditDto.setId(subreddit.getId());
        return subredditDto;
    }

    @Transactional(readOnly = true)
    public List<SubredditDto> findAll() {
        return this.subredditRepository.findAll()
                .stream()
                .map(subredditMapper::mapSubredditToDto)
                .collect(toList());
    }

    public SubredditDto findSubredditById(Long id) {
        Subreddit subreddit = this.subredditRepository.findById(id)
                .orElseThrow(() -> new SubredditNotFoundException(id));
        return this.subredditMapper.mapSubredditToDto(subreddit);
    }
}
