package com.dev.reddit.service;

import com.dev.reddit.dto.VoteDto;
import com.dev.reddit.exceptions.PostNotFoundException;
import com.dev.reddit.exceptions.SpringRedditException;
import com.dev.reddit.model.Post;
import com.dev.reddit.model.Vote;
import com.dev.reddit.repository.PostRepository;
import com.dev.reddit.repository.VoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.dev.reddit.model.VoteType.UP_VOTE;

@Service
@AllArgsConstructor
public class VoteService {
    private final VoteRepository voteRepository;
    private final PostRepository postRepository;
    private final AuthService authService;

    @Transactional
    public void vote(VoteDto voteDto) {
        final Post post = this.postRepository
                .findById(voteDto.getPostId())
                .orElseThrow(() -> new PostNotFoundException(voteDto.getPostId().toString()));
        Optional<Vote> voteByPostAndUser = this.voteRepository
                .findTopByPostAndUserOrderByVoteIdDesc(post, this.authService.getCurrentUser());
        if (voteByPostAndUser.isPresent() && voteByPostAndUser.get().getVoteType().equals(voteDto.getVoteType())) {
            throw new SpringRedditException(String.format("You have already %s'd for this post", voteDto.getVoteType()));
        }

        final Integer voteCount = post.getVoteCount();
        post.setVoteCount(UP_VOTE.equals(voteDto.getVoteType()) ? (voteCount + 1) : (voteCount - 1));
        this.voteRepository.save(mapToVote(voteDto, post));
        this.postRepository.save(post);
    }

    private Vote mapToVote(VoteDto voteDto, Post post) {
        return Vote.builder()
                .voteType(voteDto.getVoteType())
                .post(post)
                .user(this.authService.getCurrentUser())
                .build();
    }
}
