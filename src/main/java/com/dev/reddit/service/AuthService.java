package com.dev.reddit.service;

import com.dev.reddit.dto.request.LoginRequest;
import com.dev.reddit.dto.request.RefreshTokenRequest;
import com.dev.reddit.dto.request.RegisterRequest;
import com.dev.reddit.dto.response.AuthenticationResponse;
import com.dev.reddit.exceptions.TokenInvalidException;
import com.dev.reddit.exceptions.UserNotFoundException;
import com.dev.reddit.model.NotificationEmail;
import com.dev.reddit.model.User;
import com.dev.reddit.model.VerificationToken;
import com.dev.reddit.repository.UserRepository;
import com.dev.reddit.repository.VerificationTokenRepository;
import com.dev.reddit.security.JwtProvider;
import com.dev.reddit.service.mail.MailService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static com.dev.reddit.constants.Email.EMAIL_BODY;
import static com.dev.reddit.constants.Email.EMAIL_SUBJECT;

@Service
@AllArgsConstructor
public class AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final MailService mailService;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final RefreshTokenService refreshTokenService;

    @Transactional
    public void registering(RegisterRequest request) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(this.passwordEncoder.encode(request.getPassword()));
        user.setCreated(Instant.now());
        user.setEnabled(false);

        this.userRepository.save(user);

        String token = this.generateVerificationToken(user);
        this.mailService.sendMail((new NotificationEmail(
                EMAIL_SUBJECT,
                user.getEmail(),
                String.format(EMAIL_BODY, token)
        )));
    }

    private String generateVerificationToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);

        this.verificationTokenRepository.save(verificationToken);
        return token;
    }

    @Transactional
    public void verifyAccount(String token) {
        Optional<VerificationToken> verificationToken = this.verificationTokenRepository.findByToken(token);
        verificationToken.orElseThrow(TokenInvalidException::new);
        this.fetchUserAndEnable(verificationToken.get());
    }

    private void fetchUserAndEnable(VerificationToken verificationToken) {
        String username = verificationToken.getUser().getUsername();
        User user = this.userRepository
                .findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        user.setEnabled(true);
        this.userRepository.save(user);
    }

    public AuthenticationResponse login(LoginRequest request) {
        try {
            Authentication authentication = this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = this.jwtProvider.generateToken(authentication);
            return AuthenticationResponse.builder()
                    .authenticationToken(token)
                    .refreshToken(this.refreshTokenService.generateRefreshToken().getToken())
                    .expiresAt(Instant.now().plusMillis(this.jwtProvider.getJwtExpirationInMillis()))
                    .username(request.getUsername())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }

    @Transactional(readOnly = true)
    public User getCurrentUser() {
        Jwt principal = (Jwt) SecurityContextHolder.
                getContext()
                .getAuthentication()
                .getPrincipal();
        return this.userRepository.findByUsername(principal.getSubject())
                .orElseThrow(() -> new UserNotFoundException(principal.getSubject()));
    }

    public AuthenticationResponse refreshToken(RefreshTokenRequest request) {
        this.refreshTokenService.validateRefreshToken(request.getRefreshToken());
        String token = this.jwtProvider.generateTokenWithUserName(request.getUsername());
        return AuthenticationResponse.builder()
                .authenticationToken(token)
                .refreshToken(request.getRefreshToken())
                .expiresAt(Instant.now().plusMillis(this.jwtProvider.getJwtExpirationInMillis()))
                .username(request.getUsername())
                .build();
    }
}
