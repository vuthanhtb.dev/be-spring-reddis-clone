package com.dev.reddit.service;

import com.dev.reddit.dto.request.PostRequest;
import com.dev.reddit.dto.response.PostResponse;
import com.dev.reddit.exceptions.PostNotFoundException;
import com.dev.reddit.exceptions.SubredditNotFoundByNameException;
import com.dev.reddit.exceptions.SubredditNotFoundException;
import com.dev.reddit.mapper.PostMapper;
import com.dev.reddit.model.Post;
import com.dev.reddit.model.Subreddit;
import com.dev.reddit.model.User;
import com.dev.reddit.repository.PostRepository;
import com.dev.reddit.repository.SubredditRepository;
import com.dev.reddit.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class PostService {
    private final PostRepository postRepository;
    private final SubredditRepository subredditRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final PostMapper postMapper;

    public void save(PostRequest postRequest) {
        Subreddit subreddit = this.subredditRepository
                .findByName(postRequest.getSubredditName())
                .orElseThrow(() -> new SubredditNotFoundByNameException(postRequest.getSubredditName()));
        Post post = this.postMapper.map(postRequest, subreddit, this.authService.getCurrentUser());
        this.postRepository.save(post);
    }

    @Transactional(readOnly = true)
    public PostResponse findPostById(Long id) {
        Post post = this.postRepository.findById(id).orElseThrow(() -> new PostNotFoundException(id.toString()));
        return this.postMapper.mapToDto(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> findAllPosts() {
        return this.postRepository.findAll()
                .stream()
                .map(this.postMapper::mapToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> findPostsBySubreddit(Long subredditId) {
        Subreddit subreddit = this.subredditRepository
                .findById(subredditId)
                .orElseThrow(() -> new SubredditNotFoundException(subredditId));
        List<Post> posts = this.postRepository.findAllBySubreddit(subreddit);
        return posts.stream().map(this.postMapper::mapToDto).collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> findPostsByUsername(String username) {
        User user = this.userRepository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return this.postRepository
                .findByUser(user)
                .stream()
                .map(this.postMapper::mapToDto)
                .collect(toList());
    }
}
