package com.dev.reddit.exceptions;

public class SubredditNotFoundByNameException extends RuntimeException {
    public SubredditNotFoundByNameException(String name) {
        super(String.format("Subreddit not found with name: %s", name));
    }
}
