package com.dev.reddit.exceptions;

public class SubredditNotFoundException extends RuntimeException {
    public SubredditNotFoundException(Long id) {
        super(String.format("No subreddit found with ID - %d", id));
    }
}
