package com.dev.reddit.exceptions;

public class PostNotFoundException extends RuntimeException {
    public PostNotFoundException(String id) {
        super(String.format("Post not found with id: %s", id));
    }
}
