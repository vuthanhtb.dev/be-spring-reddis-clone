package com.dev.reddit.exceptions;

public class TokenInvalidException extends RuntimeException {
    public TokenInvalidException() {
        super("Invalid Token");
    }
}
