package com.dev.reddit.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {
    @NotNull(message = "Tên đăng nhập không được để trống")
    private String username;

    @NotNull(message = "Mật khẩu không được để trống")
    private String password;
}
